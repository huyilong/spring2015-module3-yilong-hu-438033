insert into students (id, first_name, last_name, email_address)
values     ('88', 'Ben', 'Harper', 'bharper@ffym.com'),
           ('202', 'Matt', 'Freeman', 'mfreeman@kickinbassist.net'),
           ('115', 'Marc', 'Roberge', 'mroberge@ofarevolution.us');



insert into courses (school_code, dept_id, course_code, name)
values 	   ('E', '81', '330S', 'Rapid Prototype Development and Creative Programming'),
           ('E', '81', '462M', 'Computer Systems Design'),
           ('E', '81', '566S', 'High Performance Computer Systems');



insert into grades (student_id, grade, school_code, dept_id, course_code)
values     ('88', '35.5', 'E', '81', '330S'),
           ('88', '0', 'E', '81', '462M'),
           ('88', '95', 'E', '81', '566S'),

           ('202', '100', 'E', '81', '330S'),
           ('202', '90.5', 'E', '81', '462M'),
           ('202', '94.8', 'E', '81', '566S'),

           ('115', '75', 'E', '81', '330S'),
           ('115', '37', 'E', '81', '462M'),
           ('115', '45.5', 'E', '81', '566S');

