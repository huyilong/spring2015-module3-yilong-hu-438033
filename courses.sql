create table courses(
	school_code enum('L', 'B', 'A', 'F', 'E', 'T', 'I', 'W', 'S', 'U', 'M') not null,
	dept_id TINYINT UNSIGNED not null,
	course_code char(5) not null,
	name VARCHAR(150) not null,
	primary key(school_code, dept_id, course_code),
	FOREIGN key (school_code, dept_id) REFERENCES departments (school_code, dept_id)
)engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;
